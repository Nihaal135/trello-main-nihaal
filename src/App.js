import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import NavBar from "./components/NavBar";

import "bootstrap/dist/css/bootstrap.min.css";

import Boards from "./components/Boards";
import List from "./components/List";


function App() {
  return (
    <React.Fragment>
    <NavBar />
     <Router>
        <Switch>
          <Route exact path = "/" component={Boards} />
          <Route path="/:id" component={List} />
        </Switch>
      </Router>
    </React.Fragment>
    );
}

export default App;
