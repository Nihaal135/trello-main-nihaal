import React from "react";
import { Link } from "react-router-dom";
import image from "./images/backgroundImage.jpeg";

function BoardDisplay(props) {
  return (
    <React.Fragment>
      <Link
        to={`/${props.board.id}`}
        style={{
          margin: "1rem",
          width: "10rem",
          height: "10rem"
        }}
      >
        <img width="100%" height="75%" src={image} alt="" />
        {props.board.name}
      </Link>
    </React.Fragment>
  )
}

export default BoardDisplay;
