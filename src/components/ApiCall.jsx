import axios from "axios";

const key = '0d162a34be4466124f18127aa975f762';
const token = '756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93';

export function getBoardData(){
    return axios.get("https://api.trello.com/1/members/me/boards?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93")
    .then(result => result.data)
}

export function addBoard(title){
    return axios.post(`https://api.trello.com/1/boards/?name=${title}&key=${key}&token=${token}`)
    .then(result => result.data)
}

export function deleteBoard(id){
    axios.delete(`https://api.trello.com/1/boards/${id}?key=${key}&token=${token}`)
}

export async function getLists(id){
    return await axios.get(`https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`)
    .then(result => result.data)
}

export async function addList(id, name){
    return await axios.post(`https://api.trello.com/1/boards/${id}/lists?name=${name}&key=${key}&token=${token}`)
    .then(result => result.data)
}

export async function deleteList(id){
    return await axios.put(`https://api.trello.com/1/lists/${id}/closed?key=${key}&token=${token}&value=true`)
}

export async function getCards(id){
    return await axios.get(`https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}`)
    .then(result => result.data)
}

export async function addCards(id, name){
    return await axios.post(`https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}&name=${name}`)
    .then(result => result.data)
}

export async function removeCards(id){
    return await axios.delete(`https://api.trello.com/1/cards/${id}?key=${key}&token=${token}`)
    .then(result => result.data)
}

export async function getCheckLists(id){
    return await axios.get(`https://api.trello.com/1/cards/${id}/checklists?key=${key}&token=${token}`)
    .then(result => result.data)
}

export async function addCheckList(id, name){
    return await axios.post(`https://api.trello.com/1/checklists?idCard=${id}&key=${key}&token=${token}&name=${name}`)
    .then(result => result.data)
}

export async function deleteCheckList(id){
    return await axios.delete(`https://api.trello.com/1/checklists/${id}?key=${key}&token=${token}`)
    .then(result => result.data)
}

export async function getCheckListItems(id){
    return await axios.get(`https://api.trello.com/1/checklists/${id}/checkItems?key=${key}&token=${token}`)
    .then(result => result.data)
}

export async function addCheckListItems(id, name){
    return await axios.post(`https://api.trello.com/1/checklists/${id}/checkItems?name=${name}&key=${key}&token=${token}`)
    .then(result => result.data)
}

export async function deleteCheckItems(listId, itemId){
    return await axios.delete(`https://api.trello.com/1/checklists/${listId}/checkItems/${itemId}?key=${key}&token=${token}`)
    .then(result => result.data)
}