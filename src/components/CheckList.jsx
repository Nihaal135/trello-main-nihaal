import React, { Component } from "react";

import axios from "axios";

import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

import CheckItems from "./CheckItem";
import * as ApiCall from "./ApiCall";

class CheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      listDatas: [],
      listName: ""
    }

    this.showModal = this.showModal.bind(this);
    
    this.hideModal = this.hideModal.bind(this);
  }

  showModal = () => {
    this.setState({ show: true });
  }

  hideModal = () => {
    this.setState({ show: false });
  }

  componentDidMount() {
    const listId = this.props.checklistId;
    //ApiCall.getCheckList()
    axios
      .get(
        `https://api.trello.com/1/cards/${listId}/checklists?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
      )
      .then((value) => {
        this.setState({
          listDatas: value.data,
        })
      })
  }

  addList = (event) => {
    event.preventDefault();
    const listId = this.props.checklistId;
    const name = this.state.listName;
    //ApiCall.addCheckList(listId, name)
    axios
      .post(
        `https://api.trello.com/1/checklists?idCard=${listId}&name=${name}&key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
      )
      .then((value) => {
        this.setState({
          listDatas: [...this.state.listDatas, value.data],
          listName: ""
        });
      });
  };

  deleteList = async (checklist) => {
    //ApiCall.deleteCheckList(checkList)
    await axios
      .delete(
        `https://api.trello.com/1/checklists/${checklist}?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
      )
      .then((res) => {
        this.setState({
          listDatas: this.state.listDatas.filter((list) => {
            if (list.id !== checklist) {
              return list
            }
          })
        })
      })
  }

  render() {
    return (
      <React.Fragment>
        <Button
          style={{ backgroundColor: "gray", margin: "10px" }}
          onClick={this.showModal}
        >
          Checklist
        </Button>

        <Modal show={this.state.show} onHide={this.hideModal}>
          <Modal.Header closeButton>
            <Modal.Title>Create Checklist</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form>
              <input
                type="text"
                value={this.state.listName}
                onChange={(event) => {
                  this.setState({ listName: event.target.value });
                }}
              ></input>
              <button onClick={(event) => this.addList(event)}>Add</button>
            </form>
            {this.state.listDatas.map((checkValue) => {
              return (
                <div
                  key={checkValue.id}
                  style={{
                    border: "1px solid black",
                    margin: "px",
                  }}
                >
                  {checkValue.name}
                  <CheckItems
                    check={checkValue}
                    cardId={this.props.checklistId}
                  />
                  <div>
                    <button
                      style={{
                        backgroundColor: "gray",
                        margin: "1px",
                      }}
                      onClick={() => this.deleteList(checkValue.id)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              )
            })}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.hideModal}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
      </React.Fragment>
    );
  }
}
export default CheckList;