import React from "react";
import axios from "axios";
import BoardDisplay from "./BoardDisplay";
import * as ApiCall from "./ApiCall";

class Boards extends React.Component {
  state = {
    listDatas: [],
    boardTitle: ""
  }

  componentDidMount() {
    // ApiCall.getBoardData()
    axios
      .get(
        "https://api.trello.com/1/members/me/boards?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93"
      )
      .then((value) => {
        this.setState({
          listDatas: value.data,
        })
      })
  }

  createBoards = (event) => {
    event.preventDefault();
    //ApiCall.addBoard(this.state.boardTitle)
    axios.post(
      `https://api.trello.com/1/boards/?name=${this.state.boardTitle}&key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
    );
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.listDatas !== this.state.listDatas) {
      //ApiCall.getBoardData()
      axios
        .get(
          "https://api.trello.com/1/members/me/boards?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93"
        )
        .then((value) => {
          this.setState({
            listDatas: value.data,
          })
        })
    }
  }

  render() {
    return (
      <div
        style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}
      >
        {this.state.listDatas.length === 0
          ? "Create a board"
          : this.state.listDatas.map((a) => {
              return <BoardDisplay key={a.id} board={a} />;
            })}
        {/* <div className="create-board"  */}
        <div
          style={{
            margin: "1rem",
            width: "10rem",
            height: "10rem",
            border: "1px solid black",
          }}
        >
          <div>
            <p>Create a new board</p>
            <form>
              <input
                value={this.state.boardTitle}
                onChange={(event) => {
                  this.setState({ boardTitle: event.target.value });
                }}
                style={{ width: "100%" }}
                type="text"
              ></input>
              <button
                style={{ margin: "10px" }}
                onClick={(event) => {
                  this.createBoards(event);
                }}
              >
                Create
              </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default Boards;
