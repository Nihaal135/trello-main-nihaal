import React, { Component } from "react";
import axios from "axios";
import CheckList from "./CheckList";
import image from "./images/backgroundImage.jpeg";
import * as ApiCall from "./ApiCall";

class Card extends Component {
  state = {
    cardDatas: [],
  }

  componentDidMount() {
    const cardId = this.props.listId;
    //ApiCall.getCards(cardId)
    axios
      .get(
        `https://api.trello.com/1/lists/${cardId}/cards?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
      )
      .then((value) =>
        this.setState({
          cardDatas: value.data,
        })
      )
  }

  addCard = () => {
    const cardName = prompt();
    const id = this.props.listId;
    //ApiCall.addCards(id, cardName)
    axios
      .post(
        `https://api.trello.com/1/lists/${id}/cards?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93&name=${cardName}`
      )
      .then((value) => {
        this.setState({
          cardDatas: [...this.state.cardDatas, value.data],
        })
      })
  }

  removeCard = async (cardId) => {
    //ApiCall.removeCards(cardId)
    await axios.delete(
      `https://api.trello.com/1/cards/${cardId}?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
    );
    const listId = this.props.listId;
    //ApiCall.getCards(listId)
    axios
      .get(
        `https://api.trello.com/1/lists/${listId}/cards?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
      )
      .then((value) =>
        this.setState({
          cardDatas: value.data,
        })
      )
  }

  render() {
    return (
      <React.Fragment>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "10rem",
            margin: "1rem",
            border: "1px solid black",
          }}
        >
          {this.props.board.name}
          <div>
            {this.state.cardDatas.map((card) => {
              return (
                <div
                  style={{
                    border: "1px solid black",
                    margin: "10px",
                  }}
                  key={card.id}
                >
                  <img
                    width="100%"
                    height="100%"
                    src={image}
                    alt="images.jpg"
                  />
                  {card.name}
                  <button
                    style={{ backgroundColor: "gray", color: "white" }}
                    onClick={() => this.removeCard(card.id)}
                  >
                    Remove Card
                  </button>
                  <div>
                    <CheckList checklistId={card.id} />
                  </div>
                </div>
              );
            })}
            <button
              style={{
                margin: "10px",
                backgroundColor: "gray",
                color: "white",
              }}
              onClick={this.addCard}
            >
              Add Card
            </button>

            <div style={{ margin: "10px" }}>
              <button
                onClick={() => this.props.archiveList(this.props.listId)}
                style={{ backgroundColor: "gray", color: "white" }}
              >
                ArchiveList
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Card;
