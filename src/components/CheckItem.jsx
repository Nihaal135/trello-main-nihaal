import React, { Component } from "react";
import axios from "axios";

import { Form } from "react-bootstrap";

class CheckItems extends Component {
  state = {
    items: [],
    itemsName: "",
    status: ""
  }

  componentDidMount() {
    const checkId = this.props.check.id;
    //ApiCall.getCheckListItems()
    axios
      .get(
        `https://api.trello.com/1/checklists/${checkId}/checkItems?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
      )
      .then((value) => {
        this.setState({
          items: value.data,
        })
      })
  }

  addItems = (event) => {
    event.preventDefault();
    const checkId = this.props.check.id;
    //ApiCall.addCheckListItems(checkId)
    axios
      .post(
        `https://api.trello.com/1/checklists/${checkId}/checkItems?name=${this.state.itemsName}&key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
      )
      .then((value) => {
        this.setState({
          items: [...this.state.items, value.data],
          itemsName: ""
        })
      })
  }

  deleteItems = async (ID) => {
    const checkId = this.props.check.id;
    //ApiCall.deleteCheckListItem(checkId, ID)
    await axios
      .delete(
        `https://api.trello.com/1/checklists/${checkId}/checkItems/${ID}?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
      )
      .then((res) => {
        this.setState({
          items: this.state.items.filter((item) => {
            if (item.id !== ID) {
              return item;
            }
          })
        })
      })
  }

  tickBox = (status, checkItemId, id) => {
    let state;
    if (status === true) {
      state = "complete";
    } else {
      state = "incomplete";
    }
    axios
      .put(
        `https://api.trello.com/1/cards/${id}/checkItem/${checkItemId}?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`,
        null,
        {
          params: { state },
        }
      )
      .then((res) => {
        let items = this.state.items.filter((update) => {
          if (update.id === res.data.id) {
            update.state = state;
          }
          return update;
        });
        this.setState({
          items
        })
      })
  }

  render() {
    return (
      <div>
        {this.state.items.map((item) => {
          if (item.state === "complete") {
            this.status = true;
          } else {
            this.status = false;
          }
          return (
            <div
              key={item.id}
              style={{
                border: "1px solid black",
                color: "black",
                fontWeight: "normal",
                width: "20%",
                display: "flex",
                justifyContent: "space-between"
              }}
            >
              <Form>
                <Form.Group>
                  <Form.Check
                    type="checkbox"
                    checked={this.status}
                    label={item.name}
                    onChange={(event) => {
                      this.tickBox(
                        event.target.checked,
                        item.id,
                        this.props.cardId
                      );
                    }}
                  ></Form.Check>
                </Form.Group>
              </Form>
              <button
                type="button"
                style={{ backgroundColor: "gray" }}
                onClick={() => this.deleteItems(item.id)}
              >
                Delete
              </button>
            </div>
          );
        })}
        <form>
          <button
            style={{
              backgroundColor: "gray",
              margin: "1px",
            }}
            onClick={(event) => {
              this.addItems(event);
            }}
          >
            Add an item
          </button>
          <input
            type="text"
            value={this.state.itemsName}
            onChange={(event) => {
              this.setState({ itemsName: event.target.value });
            }}
          />
        </form>
      </div>
    );
  }
}

export default CheckItems;
