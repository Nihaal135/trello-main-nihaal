import React from "react";
import axios from "axios";
import Card from "./Card";
import * as ApiCall from "./ApiCall";

class CardList extends React.Component {
  state = {
    list: [],
  };

  componentDidMount() {
    const id = this.props.match.params.id;
    //ApiCall.getLists(id)
    axios
      .get(
        `https://api.trello.com/1/boards/${id}/lists?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
      )
      .then((value) =>
        this.setState({
          list: value.data,
        })
      );
  }

  addList = async (event) => {
    event.preventDefault();
    const id = this.props.match.params.id;
    const name = event.target[0].value;
    //ApiCall.addList(id, name)
    await axios
      .post(
        `https://api.trello.com/1/boards/${id}/lists?name=${name}&key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93`
      )
      .then((value) => {
        this.setState({
          list: [...this.state.list, value.data],
        });
      });
  };

  archiveList = (id) => {
    //ApiCall.deleteList(id)
    axios
      .put(
        `https://api.trello.com/1/lists/${id}/closed?key=0d162a34be4466124f18127aa975f762&token=756182e43d58eb4729c1b2f5b4fcddfbcd833bf5e6a6fbaa87591e363477fd93&value=true`
      )
      .then((res) => console.log("Archived"));

    this.setState({
      listDetails: this.state.listDetails.filter((item) => {
        if (item.id !== id) {
          return item;
        }
      }),
    });
  };

  render() {
    return (
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {this.state.list.length === 0
          ? "Create a list"
          : this.state.list.map((a) => {
              return (
                <Card
                  key={a.id}
                  board={a}
                  listId={a.id}
                  archiveList={this.archiveList}
                />
              );
            })}
        <div
          style={{
            heigth: "auto",
            margin: "2rem",
            border: "1px solid black",
            width: "15rem",
            height: "10rem",
          }}
        >
          <div>
            <h5>Create a New List</h5>
            <form
              onSubmit={(event) => {
                this.addList(event);
              }}
            >
              <input style={{ width: "50%" }} type="text"></input>
              <button type="submit" style={{ margin: "10px" }}>
                Add List
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default CardList;
